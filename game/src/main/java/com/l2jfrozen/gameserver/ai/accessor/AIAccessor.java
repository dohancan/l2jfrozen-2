package com.l2jfrozen.gameserver.ai.accessor;

import com.l2jfrozen.gameserver.ai.CtrlEvent;
import com.l2jfrozen.gameserver.ai.L2CharacterAI;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.actor.position.L2CharPosition;
import com.l2jfrozen.gameserver.model.actor.stat.CharStat;
import com.l2jfrozen.gameserver.model.task.NotifyAITask;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 21:20
 */
public abstract class AIAccessor<C extends CharStat,A extends L2CharacterAI,T extends L2Character<C,A>> {
    /**
     * Return the L2Character managed by this Accessor AI.<BR>
     * <BR>
     *
     * @return the actor
     */
    public abstract T getActor();

    /**
     * Accessor to L2Character moveToLocation() method with an interaction area.<BR>
     * <BR>
     *
     * @param x      the x
     * @param y      the y
     * @param z      the z
     * @param offset the offset
     */
    public void moveTo(int x, int y, int z, int offset) {
        getActor().moveToLocation(x, y, z, offset);
    }

    /**
     * Accessor to L2Character moveToLocation() method without interaction area.<BR>
     * <BR>
     *
     * @param x the x
     * @param y the y
     * @param z the z
     */
    public void moveTo(int x, int y, int z) {
        getActor().moveToLocation(x, y, z, 0);
    }

    /**
     * Accessor to L2Character stopMove() method.<BR>
     * <BR>
     *
     * @param pos the pos
     */
    public void stopMove(L2CharPosition pos) {
        getActor().stopMove(pos);
    }

    /**
     * Accessor to L2Character doAttack() method.<BR>
     * <BR>
     *
     * @param target the target
     */
    public void doAttack(L2Character target) {
        getActor().doAttack(target);
    }

    /**
     * Accessor to L2Character doCast() method.<BR>
     * <BR>
     *
     * @param skill the skill
     */
    public void doCast(L2Skill skill) {
        getActor().doCast(skill);
    }

    /**
     * Create a NotifyAITask.<BR>
     * <BR>
     *
     * @param evt the evt
     * @return the notify ai task
     */
    public NotifyAITask<C,A,T> newNotifyTask(CtrlEvent evt) {
        return new NotifyAITask<>(evt,getActor());
    }

    /**
     * Cancel the AI.<BR>
     * <BR>
     */
    public abstract void detachAI();
}
