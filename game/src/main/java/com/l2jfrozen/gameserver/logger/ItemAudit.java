package com.l2jfrozen.gameserver.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vadim.didenko
 * 2/27/14.
 */
public class ItemAudit {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemAudit.class);
    private static ItemAudit instance;
    public static ItemAudit getInstance() {
        return instance==null?instance=new ItemAudit():instance;
    }

    public void chat(String message){
        LOGGER.info(message);
    }
}
